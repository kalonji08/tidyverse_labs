---
output:
  html_document:
    css: styles.css
  pdf_document:
    includes:
      in_header: header.tex
---

<header>
    <h1> Tidyverse tutorial notes </h1>
</header>

## Introduction

The `tidyverse` is a collection of R packages designed for data science. They share an underlying design philosophy, grammar, and data structures. Key packages include `ggplot2`, `dplyr`, `tidyr`, `readr`, `purrr`, `tibble`, and `stringr`.

## Installation

To install the `tidyverse`, run the following command in R:

```r
install.packages("tidyverse")
```

Load the package with:

```r
library(tidyverse)
```

## Hands-on Example: Data Wrangling with `dplyr`

In this section, we explore the powerful capabilities of the `dplyr` package, a cornerstone of the `tidyverse` suite designed for data manipulation tasks. `dplyr` offers a coherent set of verbs that help you perform common data manipulation operations such as filtering rows, selecting columns, rearranging data, and much more.

### What is `dplyr`?

`dplyr` is specifically crafted to facilitate data manipulation operations. It simplifies tasks such as subsetting data, aggregating information, and performing calculations across groups of data. The package is built to work effectively with both data frames and tibbles (a modern reimagining of the data frame).

### Key Functions and their syntax

Here are some of the most crucial `dplyr` functions that you should become familiar with:

- **`filter()`**: This function allows you to subset observations based on their values. For example, `filter(mtcars, mpg > 20)` will return all rows where the `mpg` (miles per gallon) value is greater than 20.
  
- **`select()`**: Use this function to narrow down a dataset to only the necessary columns. For instance, `select(mtcars, mpg, hp)` would keep only the `mpg` and `hp` (horsepower) columns.

- **`arrange()`**: This function sorts your data. You can sort in ascending order by default or in descending order using `desc()`. For example, `arrange(mtcars, desc(mpg))` sorts the cars by decreasing fuel efficiency.

- **`mutate()`**: Add new variables that are functions of existing variables. For example, `mutate(mtcars, power_to_weight = hp / wt)` adds a new column calculating the power to weight ratio.

- **`summarise()`**: Collapse many values down to a single summary. `summarise(mtcars, avg_mpg = mean(mpg))` would provide the average miles per gallon across all cars.

### Insightful tips

- **Piping with `%>%`**: One of `dplyr`'s most distinctive features is the pipe operator `%>%` that helps in chaining commands together. This means you can string several operations together in a logical sequence. For instance, you might start by filtering data, then rearranging it, and finally summarising it, all in a coherent, readable block of code.

- **Leverage `dplyr` for grouping operations**: `dplyr` is extremely efficient for operations on grouped data. Using `group_by()` along with `summarise()` can reveal insights about subsets of your data, such as calculating the average horsepower by cylinder count.

### Example with `mtcars` dataset

Let's apply some `dplyr` operations on the `mtcars` dataset. We begin by loading it and converting it to a tibble for easier manipulation:

```r
library(tidyverse)
data("mtcars")
mtcars <- as_tibble(mtcars)

```

#####  Tip: Remember `as_tibble()` to convert data frames to tibbles for easier data handling.

#### Understanding `as_tibble()`

The `as_tibble()` function is part of the `tibble` package in R, which itself is a core component of the `tidyverse`. The purpose of `as_tibble()` is to convert existing data structures, such as data frames, into tibbles, which are a modern take on data frames but with some tweaks that make them more user-friendly.

### What is a Tibble?

A tibble is a type of data frame, but it comes with enhanced features that make it more suitable for data analysis in the `tidyverse`. Tibbles are designed to be simple and to only modernise aspects of the traditional data frame that are considered limitations.

### Key Features of Tibbles

- **Printing**: Tibbles print more cleanly in the console. They show only the first ten rows and all the columns that fit on the screen, which makes them easier to inspect when working with large datasets.
- **Subsetting**: Tibbles are more predictable when subsetting. For instance, if you subset a single column, a tibble will always return a tibble, unlike data frames which might return a vector.
- **Non-standard evaluation**: Tibbles have a more consistent behaviour in non-standard evaluation contexts (like within `dplyr` functions), which can simplify programming.

### How `as_tibble()` Works

When you convert a data frame to a tibble using `as_tibble()`, the underlying data remains the same, but the behaviour and properties of the object change to those described above. This function does not alter the data itself but changes how R handles and presents the data.

#### Example

Converting a standard data frame to a tibble:

```r
# Assuming 'df' is an existing data frame
df_tibble <- as_tibble(df)

```
This conversion ensures that 'df' now benefits from the tibble-specific features, enhancing how it interacts with other `tidyverse` packages and functions.

#### Why It Matters

Using `as_tibble()` is particularly useful when you start working with data manipulation functions from `dplyr`. Tibbles are fully compatible with `dplyr` functions, ensuring that data manipulations are handled efficiently and without unexpected type coercion or other common data handling issues seen with standard data frames.

##### Tip: Embrace `as_tibble()` for a smoother, more intuitive data handling experience, especially when working within the `tidyverse`.

### Selecting columns and filtering rows

Focus on automatic cars and select specific columns:

```r
auto_cars <- mtcars %>%
  filter(am == 0) %>%
  select(mpg, hp, wt)
```

##### Checkpoint: Use `filter()` for rows and `select()` for columns. Remember, `%>%` pipes the output of one function to the input of the next.

### Mutate: adding new columns

Calculate the power-to-weight ratio:

```r
auto_cars <- auto_cars %>%
  mutate(pw_ratio = hp / wt)
```

##### Tip: `mutate()` adds new columns. Think of it as mutating or changing the dataset by adding more information.


## Assignment

Create a tibble from `mtcars` with cars having at least 100 hp, sort them by weight, and add a column `efficiency` calculated as `mpg/hp`.


<footer>
    <p></p>
</footer>

